/**
* Jie - A small, light, clean, and fast JS library.
* (c) Lee Caine 2016+
* MIT License
**/

/**
* Console.log
**/
var print = function(str) {
	console.log(str);
};

/**
* Extend a few built in types with useful methods.
**/

/**
* Allow for quick if else on boolean value, like:
*
*	var someVal = true;
*	someVal.then(function() {}).otherwise(function({});
**/
Boolean.prototype.then = function(func) {
	// Execute func if true
	if(func instanceof Function) {
		if(this == true) {
			func();
		}
		return this;
	}else {
		throw "The if() method parameter should be a function to call if the value is true.";
	}
};
Boolean.prototype.otherwise = function(func) {
	// Execute func if false
	if(func instanceof Function) {
		if(this == false) {
			func();
		}
		return this;
	}else {
		throw "The else() method parameter should be a function to call if the value is false.";
	}
};

/**
* Useful string methods
**/
String.prototype.times = function(func) {
	// Call func this amount of times, passing increment as argument
	try {
		var intval = parseInt(this);
	}catch(err) {
		throw "You can only use times() on a string that is a single integer.";
	}
	for(var i = 1; i < (intval + 1); i++) {
		func(i);
	}
	return this;
};
String.prototype.contains = function(otherString) {
	if(_jieIsTypeOf(otherString, "String")) {
		if(this.indexOf(otherString) !== -1) {
			return true;
		}else {
			return false;
		}
	}else {
		throw "The contains() method should take a string as parameter.";
	}
};
String.prototype.equals = function(otherString) {
	return (this == otherString);
};
String.prototype.startsWith = function(otherString) {
	// Does this string start with otherString?
	if(_pieIsTypeOf(otherString, "String")) {
		if(this.lastIndexOf(otherString, 0) === 0) {
			return true;
		}else {
			return false;
		}
	}else {
		throw "The startsWith() method should take a string as parameter.";
	}
};
String.prototype.endsWith = function(otherString) {
	// Does this string end with otherString?
	if(_pieIsTypeOf(otherString, "String")) {
		if(this.indexOf(otherString, this.length - otherString.length) !== -1) {
			return true;
		}else {
			return false;
		}
	}else {
		throw "The endsWith() method should take a string as parameter.";
	}
};
String.prototype.join = function(array) {
	// Joins the items in the given array using this as separator.
	var str = "";
	for(var i = 0; i < array.length; i++) {
		str = str + array[i];
		if(i < (array.length - 1)) {
			str = str + this;
		}
	}
	return str;
};
String.prototype.lower = function() {
	// Just a shortcut for toLowerCase()
	return this.toLowerCase();
};
String.prototype.upper = function() {
	// Just a shortcut for toUpperCase()
	return this.toUpperCase();
};
String.prototype.capitalize = function() {
	return this.charAt(0).upper() + this.slice(1);
};
String.prototype.print = function() {
	// Print this string
	print(this);
	return this;
};

Array.prototype.contains = function(object) {
	// Does the array contain object?
	if(this.indexOf(object) !== -1) {
		return true;
	}
	return false;
};
Array.prototype.size = function() {
	return this.length;
};
Array.prototype.explode = function(separator) {
	// Create a string from the items of an array separated by separator
	if(_pieIsTypeOf(separator)) {
		return separator.join(this);
	}else {
		throw "explode() Takes a string as parameter.";
	}
};
Array.prototype.loop = function(func) {
	// Call func for each item in this array passing
	// the item as first parameter and the incrementer the second.
	if(_jieIsTypeOf(func, "Function")) {
		for(var i = 0; i < this.length; i++) {
			func(this[i], i);
		}
		return this;
	}else {
		throw "loop() takes a function as parameter.";
	}
};

/**
* Sure fire way of checking object type.
**/
var _jieIsTypeOf = function(object, type) {
	if(typeof object === type.toLowerCase()) {
		return true;
	}
	var typeStr = Object.prototype.toString.call(object);
	if(typeStr === "[object " + type + "]") {
		return true;
	}
	return false;
};

/**
* Cross browser attach event method.
**/
var _jieAttachEvent = function(element, event, func) {
	if(element.addEventListener) {
		element.addEventListener(event, func, false);
	}else {
		element.attachEvent("on" + event, function() {
			return (func.call(element, window.event));
		});
	}
};

/**
* A pie element class, wraps an HTMl element and provides
* all the Pie goodness to it.
**/
var _JieElement = function(htmlElement) {
	if(htmlElement instanceof HTMLElement) {
		this._htmlElement = htmlElement;
		return this;
	}else {
		throw "You can only create a _PieElement with an Element.";
	}
};
_JieElement.prototype.attributes = function(param1, param2) {
	// Get or set an elements attributes based on parameter types..
	if(_jieIsTypeOf(param1, "String") && _jieIsTypeOf(param2, "String")) {
		// Set single
		this._htmlElement.setAttribute(param1, param2);
		return this;
	} else if(_jieIsTypeOf(param1, "String")) {
		// Get
		var all = this.attributes();
		if(param1 in all) {
			return all[param1];
		}else {
			return null;
		}
	}else if(_jieIsTypeOf(param1, "Object")) {
		// Set multiple
		for(key in param1) {
			this.attributes(key, param1[key]);
		}
		return this;
	}else if(param1 === undefined) {
		// Get all
		var attrs = {};
		for(var i = 0; i < this._htmlElement.attributes.length; i++) {
			attrs[this._htmlElement.attributes[i].name] = this._htmlElement.attributes[i].value;
		}
		return attrs;
	}else {
		throw "attributes() Either returns a single attribute by given name, all attributes if no parameter is passed, sets a single attribute if two strings are passed, or sets multiple attributes if an associative array is passed.";
	}
};
_JieElement.prototype.hasAttribute = function(name) {
	// Does element have an attribute?
	return !(this.attributes(name) === null);
};
_JieElement.prototype.styles = function(param1, param2) {
	// Get or set an elements styles based on parameter types..
	if(param1 === undefined) {
		// Return all...
		var styles = {};
		var stylesString = this.attributes("style");
		if(stylesString === null) {
			stylesString = "";
		}
		var statements = stylesString.split(";");
		for(var i = 0; i < statements.length; i++) {
			var statement = statements[i].split(":");
			if(statement.length == 2) {
				var property = statement[0].trim();
				var value = statement[1].trim();
				styles[property] = value;
			}
		}
		return styles;
	}else if(_jieIsTypeOf(param1, "String") && _jieIsTypeOf(param2, "String")) {
		// Set single..
		var styles = this.styles();
		styles[param1] = param2;
		this.styles(styles);
		return this;
	}else if(_jieIsTypeOf(param1, "String")) {
		// Return single
		var styles = this.styles();
		if(param1 in styles) {
			return styles[param1];
		}else {
			return null;
		}
	}else if(_jieIsTypeOf(param1, "Object")) {
		// set multiple
		var styleItems = []
		for(property in param1) {
			styleItems.push(property + ":" + param1[property]);
		}
		var styleString = ";".join(styleItems);
		this.attributes("style", styleString + ";");
		return this;
	}else {
		throw "styles() takes either single style property, an associate array to set those styles, or a property and a value.";
	}
};
_JieElement.prototype.appendTo = function(element) {
	// Append to another element
	if(element instanceof _PieElement) {
		element._htmlElement.appendChild(this._htmlElement);
		return this;
	}else {
		throw "appendTo() takes a _JieElement as parameter.";
	}
};
_JieElement.prototype.prependTo = function(element) {
	// Prepend to another element
	if(element instanceof _JieElement) {
		if(element._htmlElement.firstChild !== undefined) {
			element._htmlElement.insertBefore(this._htmlElement, element._htmlElement.firstChild);
		}else {
			this.appendTo(element);
		}
		return this;
	}else {
		throw "prependTo() takes a _JieElement as parameter.";
	}
};
_JieElement.prototype.hasClass = function(className) {
	// Does element have a class?
	var classes = this.attributes('class').split(' ');
	return classes.contains(className);
};
_JieElement.prototype.classes = function(param) {
	// Get or set an elements classes
	if(param === undefined) {
		// Return all
		return this.attributes('class').split(' ');
	}else if(_jieIsTypeOf(param, "String")) {
		// Add a class
		this.classes([param]);
		return this;
	}else if(_jieIsTypeOf(param, "Array")) {
		// Set classes
		var classes = this.classes();
		param.loop(function(item) {
			if(!classes.contains(item)) {
				classes.push(item);
			}
		});
		this.attributes("class", " ".join(classes));
		return this;
	}else {
		throw "classes() Takes either a single string to add a class, or an array of strings to set. Call it with no paramaters for a list. You can also use hasClass(), addClass(), and removeClass().";
	}
};
_JieElement.prototype.addClass = function(className) {
	// Add a class
	this.classes(className);
};
_JieElement.prototype.removeClass = function(className) {
	// Remove a class
	var classes = this.classes();
	if(classes.contains(className)) {
		var newClasses = [];
		classes.loop(function(item) {
			if(item != className) {
				newClasses.push(item);
			}
		});
		this.attributes("class", " ".join(newClasses));
	}
	return this;
};
_JieElement.prototype.on = function(event, func) {
	// Attach an event listener to this element
	_jieAttachEvent(this._htmlElement, event, func);
	return this;
};
_JieElement.prototype.children = function(selector) {
	// Returns all children or filtered by passed selector.
	if(selector === null || selector === undefined) {
		var children = [];
		for(var i = 0; i < this._htmlElement.childNodes.length; i++) {
			var child = new _JieElement(this._htmlElement.childNodes[i]);
			children.push(child);
		}
		return new _JieElementList(children);
	}else {
		 return jie(this._htmlElement.querySelectorAll(selector));
	}
};

/**
* A list of Pie elements.
**/
var _JieElementList = function(jieElements) {
	if(_jieIsTypeOf(jieElements, "Array")) {
		this._jieElements = jieElements;
		return this;
	}else {
		throw "You can only create _JieElementList with an Array.";
	}
};
_JieElementList.prototype.size = function() {
	// Return length of list..
	return this._jieElements.length;
};
_JieElementList.prototype.isEmpty = function() {
	// Is this list empty?
	return (this.size() == 0);
};
_JieElementList.prototype.isntEmpty = function() {
	// Does list have items?
	return (this.size() > 0);
};
_JieElementList.prototype.first = function() {
	// Return first item
	if(this.isntEmpty()) {
		return this._jieElements[0];
	}else {
		throw "Jie List is empty, can not access first item. Use isEmpty() and isntEmpty() to determine beforehand.";
	}
};
_JieElementList.prototype.last = function() {
	// Return last item
	if(this.isntEmpty()) {
		return this._jieElements[this.size() - 1];
	}else {
		throw "Pie list is empty, can not access last item. Use isEmpty() and isntEmpty() to determine beforehand.";
	}
};
_JieElementList.prototype.get = function(index) {
	// Return the element at the specified index
	// Also allows for negative indexing
	if(typeof index === 'number' && (index % 1) === 0) {
		if((this.size() - 1) >= index) {
			// Allow for negative indexing
			if(index < 0) {
				index = (this.size() - 1) + index;
			}
			return this._jieElements[index];
		}else {
			throw "Jie list index out of range.";
		}
	}else {
		throw "The Jie list get() method takes an integer as parameter.";
	}
};
_JieElementList.prototype._appendElement = function(jieElement) {
	// Append an element to this list.
	if(jieElement instanceof _JieElement) {
		this._jieElements.push(jieElement);
		return this;
	}else {
		throw "You can only append a _JieElement to a _JieElementList.";
	}
};
_JieElementList.prototype.loop = function(func) {
	// Loop through each item in the list and call func
	// with the element as first parameter and the index
	// as second parameter
	for(var i = 0; i < this._jieElements.length; i++) {
		var jieElement = this._jieElements[i];
		func(jieElement, i);
	}
	return this;
};
_JieElementList.prototype.contains = function(jieElement) {
	// Does the list contain element?
	if(jieElement instanceof _JieElement) {
		for(var i = 0; i < this._jieElements.length; i++) {
			var element = this._jieElements[i];
			if(element._htmlElement === jieElement._htmlElement) {
				return true;
			}
		}
		return false;
	}else {
		throw "The contains() method takes a _JieElement as parameter.";
	}
};
_JieElementList.prototype.removeClass = function(param) {
	this.loop(function(element) {
		element.removeClass(param);
	});
	return this;
};
_JieElementList.prototype.attributes = function(param1, param2) {
	// Calls attributes on each _PieElement in list..
	this.loop(function(element) {
		element.attributes(param1, param2);
	});
	return this;
};
_JieElementList.prototype.styles = function(param1, param2) {
	// Calls styles on each _PieElement in the list..
	this.loop(function(element) {
		element.styles(param1, param2);
	});
	return this;
};
_JieElementList.prototype.appendTo = function(element) {
	// Calls appendTo on each _PieElement in the list
	this.loop(function(child) {
		child.appendTo(element);
	});
	return this;
};
_JieElementList.prototype.prependTo = function(element) {
	// Calls prependTo on each _PieElement in the list
	this.loop(function(child) {
		child.prependTo(element);
	});
	return this;
};
_JieElementList.prototype.on = function(event, func) {
	// Attach event listener to all elements
	this.loop(function(element) {
		element.on(event, func);
	});
};

/**
* The main function in pie, it serves several purposes.
*	- Pass it a string ( a CSS query selector ) then it
*		returns matched elements.
*	- Pass it an html element and it applies all the Pie
*		goodness to the element.
*/
var jie = function(param) {
	if(_jieIsTypeOf(param, "String")) {
		return jie(document.querySelectorAll(param));
	}else if(_jieIsTypeOf(param, "NodeList") || _jieIsTypeOf(param, "HTMLCollection") || _jieIsTypeOf(param, "Array")) {
		var jieElementList = new _JieElementList([]);
		for(var i = 0; i < param.length; i++) {
			var jieElement = new _JieElement(param[i]);
			jieElementList._appendElement(jieElement);
		}
		return jieElementList;
	}else if(_jieIsTypeOf(param, "HTMLElement")) {
		return new _JieElement(param);
	}else if(_jieIsTypeOf(param, "Function")) {
		(function() {
			_jieAttachEvent(document, "DOMContentLoaded", param);
		}());
	}else {
		throw "The jie function takes one of the following parameters: String(CSS query selector), A NodeList, An array of Elements, a Function to execute when the document is ready, or a single Element.";
	}
};